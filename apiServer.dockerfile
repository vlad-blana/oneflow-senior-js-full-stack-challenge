FROM node:latest

RUN mkdir -p /usr/src/api
WORKDIR /usr/src/api

COPY package*.json ./

RUN npm install -g nodemon
RUN npm install

EXPOSE 3001:3001