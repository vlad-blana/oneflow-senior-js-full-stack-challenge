# oneflow-senior-js-full-stack-challenge

Task 1

1. Run 'docker-compose up' to create the docker container

2. After the server has started, execute 'npm run demo' to see and check the newly generated 'thumbnails' folder for the thumbnail (without extension) and the job requests are stored at 'https://oneflowsjsfullstack.firebaseio.com/jobRequests.json?print=pretty'
- the 'thumbnails' folder is located in the project root
- the demo does a http post to 'http://localhost/api/thumnail' with a good image url found using google

3. Execute 'npm test' to run the unit tests. This requires the docker to run.

Notices!
The TODO comments are meant to signal that the code would be improved for reallife projects. They are intetionally not implemented.

Task 2

See 'Task 2 analysis.js' . It contains comments to possible issues/improvements.