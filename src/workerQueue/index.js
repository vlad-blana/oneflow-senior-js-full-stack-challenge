const Queue = require('better-queue');
const fs = require('fs');

const axios = require('../axios');
const imageThumbnail = require('../thumbnail');

const thumbnailsDir = 'thumbnails';

if (!fs.existsSync(thumbnailsDir)){
    fs.mkdirSync(thumbnailsDir);
}

const q = new Queue(async function (jobRequest) {
    try {
        const thumbnail = await imageThumbnail({ uri: jobRequest.url }, { width: 64, height: 64});
        // TODO a better file name/storage
        // TODO get and store image type information
        fs.writeFile(`${thumbnailsDir}/${jobRequest.id}`, thumbnail, async err => {
            if (err) {
                console.log(err);
                await axios.patch(`/jobRequests/${jobRequest.id}.json`, { status: 'done'});
                // TODO: if res.status !== 200
            }

            await axios.patch(`/jobRequests/${jobRequest.id}.json`, { status: 'failed'});
            // TODO: if res.status !== 200
        });
    } catch(err) {
        //console.log(err);
        await axios.patch(`/jobRequests/${jobRequest.id}.json`, { status: 'failed'});
        // TODO: if res.status !== 200
    }
});

module.exports = q;