const kue = require('kue')
const imageType = require('image-type');
const fs = require('fs');

const axios = require('./axios');
const imageThumbnail = require('./thumbnail');

const queue = kue.createQueue({
    redis: {
        host: 'redis',
        port: process.env.REDIS_PORT || 6379
    }
});

const thumbnailsDir = 'thumbnails';

if (!fs.existsSync(thumbnailsDir)){
    fs.mkdirSync(thumbnailsDir);
}

queue.process('thumbnail', async (jobRequest, done) => {
    try {
        const thumbnail = await imageThumbnail({ uri: jobRequest.data.url }, { width: 64, height: 64});
        const thumbnailType = imageType(thumbnail);
        // TODO a better file name/storage
        // TODO get and store image type information
        const fileName = `${jobRequest.data.id}.${thumbnailType.ext}`;
        fs.writeFile(`${thumbnailsDir}/${fileName}`, thumbnail, async err => {
            if (err) {
                //console.log(err);
                await axios.patch(`/jobRequests/${jobRequest.data.id}.json`, { status: 'failed'});
                // TODO: if res.status !== 200
                done();
            } else {
                await axios.patch(`/jobRequests/${jobRequest.data.id}.json`, { status: 'done', fileName});
                // TODO: if res.status !== 200
                done();
            }
        });
    } catch(err) {
        //console.log(err);
        await axios.patch(`/jobRequests/${jobRequest.data.id}.json`, { status: 'failed'});
        // TODO: if res.status !== 200
        done(new Error('unknown error!'));
    }
});