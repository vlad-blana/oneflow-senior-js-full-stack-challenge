const axios = require('axios');
const sizeOf = require('image-size');
const sharp = require('sharp');

const PERCENTAGE = 10;
const RESPONSE_TYPE = 'buffer';

const fromUri = async (source, percentage = PERCENTAGE, width, height, responseType = RESPONSE_TYPE) => {
    const response = await axios.get(source.uri, { responseType: 'arraybuffer' });
    const imageBuffer = Buffer.from(response.data, 'binary');

    const dimensions = getDimensions(imageBuffer, percentage, { width, height });
    const thumbnailBuffer = await sharpResize(imageBuffer, dimensions);


    if (responseType === 'base64') {
        return thumbnailBuffer.toString('base64');
    }

    return thumbnailBuffer;
};

const getDimensions = (imageBuffer, percentageOfImage, dimensions) => {
    if (typeof dimensions.width != 'undefined' && typeof dimensions.height != 'undefined') {
        return { width: dimensions.width, height: dimensions.height };
    }

    dimensions = sizeOf(imageBuffer);

    let width = parseInt((dimensions.width * (percentageOfImage / 100)).toFixed(0));
    let height = parseInt((dimensions.height * (percentageOfImage / 100)).toFixed(0));

    return { width, height };
}

const sharpResize = (imageBuffer, dimensions) => {
    return new Promise(function (resolve, reject) {
        sharp(imageBuffer)
            .resize(dimensions.width, dimensions.height)
            .withoutEnlargement()
            .toBuffer((err, data, info) => {
                if (err) {
                    reject(err);
                } else {
                    const { format, width, height, size } = info;
                    const imagePayload = {
                        format: format,
                        width: width,
                        height: height,
                        size: size,
                    };
                    resolve(data);
                }
            });
    });
};

module.exports = async function (source, options) {
    let percentage = options && options.percentage ? options.percentage : PERCENTAGE;
    let width = options && options.width ? options.width : undefined;
    let height = options && options.height ? options.height : undefined;
    let responseType = options && options.responseType ? options.responseType : RESPONSE_TYPE;

    try {
        // check source
        switch (typeof source) {
            case 'object':
                return await fromUri(source, percentage, width, height, responseType);
            default:
                throw new Error('unsupported source type');
        }
    } catch (err) {
        throw new Error(err.message);
    }
};