const validator = require('validator');
const axios = require('../../axios');
const kue = require('kue')

const queue = kue.createQueue({
    redis: {
        host: 'redis',
        port: process.env.REDIS_PORT || 6379
    }
});

const doThumbnail = (req, res) => {
    if (!req.is('application/json')) res.sendStatus(415);
    if (!req.body.url) res.sendStatus(422);
    if (!validator.isURL(req.body.url)) req.sendStatus(422);

    axios.post('/jobRequests.json', { status: 'pending' })
        .then(response => {
            queue.create('thumbnail', {
                id: response.data.name,
                url: req.body.url
            }).save();

            res.sendStatus(200);
        })
        .catch(error => {
            console.log(error); 
            res.sendStatus(500);
        });
}

module.exports = {
    doThumbnail
};