const express = require('express');
const { doThumbnail } = require('./resolvers');

const router = express.Router();

router.post('/thumbnail', doThumbnail);

module.exports = router;
