const express = require('express');
const router = require('./routes');
const bodyParser = require('body-parser');

const createApp = () => {
    app = express();

    app.use(bodyParser.urlencoded({ extended: false }));
    app.use(bodyParser.json())
    app.use("/api", router);

    return app;
}

module.exports = createApp;