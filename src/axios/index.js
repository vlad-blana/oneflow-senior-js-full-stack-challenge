const axios = require('axios');

module.exports = axios.create({
    baseURL: 'https://oneflowsjsfullstack.firebaseio.com/'
});

