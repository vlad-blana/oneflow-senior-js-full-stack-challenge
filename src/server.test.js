const request = require('supertest');
const { expect } = require('chai');
//const createApp = require('./expressServer');
const axios = require('axios');

const fs = require('fs');

function sleep (time) {
    return new Promise(resolve => setTimeout(resolve, time));
}

describe('express server', () => {
    // let app;

    // before(function(done) {
        // app = createApp();
        // app.listen(function(err) {
        //   if (err) { return done(err); }
        //   done();
        // });
    // });

    it('should thumbnail from good url return ok!', (done) => {
        request('')
            .post('http://localhost/api/thumbnail')
            .set('Content-Type', 'application/json')
            .send({ url: 'https://images.pexels.com/photos/67636/rose-blue-flower-rose-blooms-67636.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260' })
            //.expect('Content-Type', /json/)
            .expect(200, async (err, res) => {
                if (err) { return done(err); }
                //expect(res.body.message).to.equal('Thumbnail!' );

                try {
                    const axiosRes = await axios.get('https://oneflowsjsfullstack.firebaseio.com/jobRequests.json?print=pretty');

                    // dirty - usually only the last would have to be returned but firebase doesn't give this without orderby
                    const dataKeys = Object.keys(axiosRes.data);
                    const jobId = dataKeys[dataKeys.length - 1];
                    const ofInterest = axiosRes.data[jobId];
                    expect(ofInterest.status).to.equal('pending');
                    
                    await sleep(5000);

                    const axiosRes2 = await axios.get('https://oneflowsjsfullstack.firebaseio.com/jobRequests.json?print=pretty');

                    const dataKeys2 = Object.keys(axiosRes2.data);
                    const jobId2 = dataKeys2[dataKeys2.length - 1];

                    expect(jobId2).to.equal(jobId);

                    const ofInterest2 = axiosRes2.data[jobId2];
                    expect(ofInterest2.status).to.equal('done');
                    
                    // thumbnail check - can't be done without
                    expect(fs.existsSync(`thumbnails/${ofInterest2.fileName}`)).to.equal(true);
                    done();
                } catch(err2) {
                    console.log(err2);
                    done(new Error(err2));
                }
            });
    }).timeout(10000);

    it('should thumbnail from bad url fail!', (done) => {
        request('')
            .post('http://localhost/api/thumbnail')
            .set('Content-Type', 'application/json')
            .send({ url: 'https://images.pexels.com/photos/' })
            .expect(200, async (err, res) => {
                if (err) { return done(err); }
                try {
                    const axiosRes = await axios.get('https://oneflowsjsfullstack.firebaseio.com/jobRequests.json?print=pretty');

                    // dirty - usually only the last would have to be returned but firebase doesn't give this without orderby
                    const dataKeys = Object.keys(axiosRes.data);
                    const jobId = dataKeys[dataKeys.length - 1];
                    const ofInterest = axiosRes.data[jobId];
                    expect(ofInterest.status).to.equal('pending');
                    
                    await sleep(5000);

                    const axiosRes2 = await axios.get('https://oneflowsjsfullstack.firebaseio.com/jobRequests.json?print=pretty');

                    const dataKeys2 = Object.keys(axiosRes2.data);
                    const jobId2 = dataKeys2[dataKeys2.length - 1];

                    expect(jobId2).to.equal(jobId);

                    const ofInterest2 = axiosRes2.data[jobId2];
                    expect(ofInterest2.status).to.equal('failed');
                    
                    // thumbnail check - can't be done without
                    expect(fs.existsSync(`thumbnails/${jobId2}`)).to.equal(false);
                    done();

                } catch(err2) {
                    console.log(err2);
                    done(new Error(err2));
                }
            });
    }).timeout(10000);
});