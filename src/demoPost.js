const axios = require('axios');

const main = async () => {
    const payload = {
        "url": "https://images.pexels.com/photos/67636/rose-blue-flower-rose-blooms-67636.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260"
    };
    const response = await axios.post('http://localhost/api/thumbnail', payload);
    console.log(response);
};

main();