// Ideally should not reuse err in every subsequent callbacks
exports.inviteUser = function (req, res) {
    var invitationBody = req.body;
    var shopId = req.params.shopId;
    var authUrl = " https://url.to.auth.system.com/invitation ";
    superagent
        .post(authUrl)
        .send(invitationBody)
        .end(function (err, invitationResponse) {
            // Maybe should use err?
            if (invitationResponse.status === 201) {
                User.findOneAndUpdate({
                    authId: invitationResponse.body.authId
                }, {
                        authId: invitationResponse.body.authId,
                        email: invitationBody.email
                    }, {
                        upsert: true,
                        new: true
                    }, function (err, createdUser) {
                        // Need await maybe here?
                        Shop.findById(shopId).exec(function (err, shop) {
                            if (err || !shop) {
                                return res.status(500).send(err || { message: 'No shop found' });
                            }
                            // This should compared against === -1 since normally it will insert the id when it exists
                            if (shop.invitations.indexOf(invitationResponse.body.invitationId)) {
                                shop.invitations.push(invitationResponse.body.invitationId);
                            }
                            if (shop.users.indexOf(createdUser._id) === - 1) {
                                shop.users.push(createdUser);
                            }
                            shop.save();
                            // Insert res.json(invitationResponse); here ?
                        });
                    });
                // Response if it gets here without a response?
            } else if (invitationResponse.status === 200) {
                res.status(400).json({
                    error: true,
                    message: 'User already invited to this shop'
                });
                return;
            }
            // Else? so it won't be called again for the other cases?
            res.json(invitationResponse);
        });
};

// Promise variant