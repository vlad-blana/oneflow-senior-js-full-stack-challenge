FROM node:latest

RUN mkdir -p /usr/src/worker
WORKDIR /usr/src/worker

COPY package*.json ./

RUN npm install -g nodemon
RUN npm install